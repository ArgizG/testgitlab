TOTAL_FILES = 2345
FILE_CONTENT = "File Content"

TOTAL_FILES.times { |i|
    File.open("files/fileName_#{i}.txt", 'w') do |f|
        f.write(FILE_CONTENT)
    end
}